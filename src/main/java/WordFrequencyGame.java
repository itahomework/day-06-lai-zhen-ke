import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WordFrequencyGame {
    private final static String CALCULATE_ERROR = "Calculate Error";
    private final static String SPLIT_REGEX = "\\s+";
    public String countWordsFrequency(String string) {
        try {
            List<Word> wordList = generateWordList(string.split(SPLIT_REGEX));
            wordList.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());
            return wordList.stream().map(word -> word.getValue() + " " + word.getWordCount()).collect(Collectors.joining("\n"));
        } catch (Exception e) {
            return CALCULATE_ERROR;
        }
    }

    private List<Word> generateWordList(String[] stringArray) {
        List<Word> wordList = new ArrayList<>();
        for (String string : stringArray) {
            Word word = new Word(string, 1);
            if (wordList.contains(word)) {
                wordList.get(wordList.indexOf(word)).addWordCount();
            } else {
                wordList.add(word);
            }
        }
        return wordList;
    }
}
