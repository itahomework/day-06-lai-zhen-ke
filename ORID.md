### O:

- In code review, my code was selected for presentation, I didn't consider actual application scenarios in order to use design patterns, so sometimes I write extra code. 
- In the morning, I also shared the PPT of design patterns of each group. I had a deeper understanding of observer pattern and command pattern.
- In the afternoon, I learned how to refactor code.

### R: 

- Benefit a lot.

### I: 

- When typing code, consider the actual application scenario and do not abuse design patterns. However, design patterns can decouple code and improve maintainability, and it is necessary to find a balance between the them.

### D:

- Go deeper into learning design patterns and understand when to use them.